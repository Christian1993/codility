// 1 Binary gap
// find longest gap in binary number for example:
// d: 8 b: 1000 gap: 0
// d: 9 b: 1001 gap: 2

// IN: an integer within the range [1..2,147,483,647]
// OUT: an integer

function binaryGap(n) {
  const bin = n.toString(2).split('').reverse();
  let currentGap = 0;
  let maxGap = 0;
  let firstIndex = 0;

  for (let i = 0, len = bin.length; i < len; i++) {
    if (bin[i] === '1') {
      firstIndex = i;
      break;
    }
  }

  for (let i = firstIndex, len = bin.length; i < len; i++) {
    if (bin[i] === '1') {
      maxGap = currentGap > maxGap ? currentGap : maxGap;
      currentGap = 0;
    } else {
      currentGap++;
    }
  }
  return maxGap;
}

// console.log(binaryGap(17)); // 17 = 10001 <- 3
// console.log(binaryGap(8)); // 8 = 1000 <- 0

// 2.1 CyclicRotation
// shift elements in array by K positions for example:
//  [1, 2, 3, 4] K = 1 [4, 1, 2, 3]
//  [1, 2, 3, 4] K = 2 [3, 4, 1, 2]
// IN: K an integer within the range [0...100]
// each element of array A is an integer within the range [−1,000..1,000]
// OUT: an array

function cyclicRotation(array, k) {
  for (let j = 0; j < k; j++) {
    let lastItem = array[array.length - 1];
    for (let i = array.length - 1; i >= 0; i--) {
      if (i === 0) {
        array[i] = lastItem;
      } else {
        array[i] = array[i - 1];
      }
    }
  }
  return array;
}

// const arr1 = [1, 2, 3, 4];
// const arr2 = [1, 2, 3, 4];
// const arr3 = [1, 2, 3, 4];
// console.log(cyclicRotation(arr1, 0)); // [ 1, 2, 3, 4 ]
// console.log(cyclicRotation(arr2, 1)); // [ 4, 1, 2, 3 ]
// console.log(cyclicRotation(arr3, 3)); // [ 2, 3, 4, 1 ]

// 2.2 OddOccurrencesInArray
// find value of element, that has no pair
// IN: an array with odd number of elements:
// A[0] = 9, A[4] = 9 <- paired
// A[1] = 5, A[2] = 5 <- paired
// A[3] = 7 <- unpaired, find this one
// OUT an integer (value of an element)

function oddOccurrencesInArray(array) {
  const obj = {};

  for (const num of array) {
    if (obj[num]) {
      obj[num]++;
    } else {
      obj[num] = 1;
    }
  }

  for (const key in obj) {
    if (obj[key] % 2 === 1) {
      return parseInt(key);
    }
  }
}

// const arr = [1, 10, 17, 10, 17, 1, 17]; // 17
// console.log(oddOccurrencesInArray(arr));

// 3.1 PermMissingElem
// find missing element in an array
// [2, 1, 3, 5] <- 4 is missing
// IN: an array [1...N + 1]
//

function permMissingElem(array) {
  const len = array.length + 1;
  const sumGiven = array.reduce((acc, val) => acc + val, 0);
  const sumInTheory = len * (len + 1) / 2;
  return sumInTheory - sumGiven;
}

// const arr = [2, 1, 3, 5];
// const arr2 = [2, 4, 3, 5];
// const arr3 = [1];
// const arr4 = [];

// console.log(permMissingElem(arr)); // 4
// console.log(permMissingElem(arr2)); // 1
// console.log(permMissingElem(arr3)); // 2
// console.log(permMissingElem(arr4)); // 1

// 3.2 FrogJmp
// count minimal number of jumps from position x to y
// X = 10, Y = 85, D = 30
// 1. 10 + 30 = 40
// 2. 40 + 30 = 70
// 3. 70 + 30 = 100 (3 jumps)
// IN: X - current position, Y - position to achieve, D - jump length
// OUT: an integer, number of jumps

function frogJmp(x, y, d) {
  return Math.ceil((y - x) / d);
}

// console.log(frogJmp(10, 85, 30)); // 3
// console.log(frogJmp(70, 70, 30)); // 0
// console.log(frogJmp(10, 70, 5000)); // 1

// 3.3 TapeEquilibrium
// find min value
// Example: There is an array given: [3, 2, 5, 7]
// it can be split in 3 places P1, P2, P3 ( | )
// after splitting, sum created parts and subtract them
// find min value
// P1: 3 | 2, 5, 7   |3 - 14| = 11
// P2: 3, 2 | 5, 7   |5 - 12| = 7
// P3: 3, 2 , 5 | 7  |10 - 7| = 3 <- this one wins
// IN: an integer within the range [2...100 000],
// each element in array is within range [-1000...1000]
// OUT: an integer (minimal difference)

function tapeEquilibrium(array) {
  const sum = array.reduce((acc, val) => acc + val, 0);
  let left = array[0];
  let right = sum - left;
  let p_min = Math.abs(left - right);
  let p;

  for (let i = 1, len = array.length - 1; i < len; i++) {
    left += array[i];
    right = sum - left;
    p = Math.abs(left - right);
    p_min = p_min < p ? p_min : p;
  }
  return p_min;
}

// const arr = [3, 2, 5, 7];
// const arr2 = [-100, 100];
// console.log(tapeEquilibrium(arr)); // 3
// console.log(tapeEquilibrium(arr2)); // 200

// 4.1 PermCheck
// check if an array is a permutation
// A permutation is a sequence containing each element from 1 to N once, and only once.
// example: [4, 3, 1, 2] is ok
// [4, 5, 1, 2] is not ok (3 is missing)
// IN: an array
// OUT: 1 if array is a permutation, 0 if isn't

function permCheck(array) {
  array.sort((a, b) => a - b);
  for (let i = 0, len = array.length; i < len; i++) {
    if (array[i] !== i + 1) {
      return 0;
    }
  }
  return 1;
}

// const arr1 = [4, 5, 1, 2];
// const arr2 = [4, 3, 1, 2];
//
// console.log(permCheck(arr1)); // 0
// console.log(permCheck(arr2)); // 1

// 4.2 FrogRiverOne
// find the earliest time when a frog can jump to the other side of a river
// you are given river width X and array  A[time] = leaf_position (they are falling from the tree)
// example X: 3 [1, 2, 1, 3, 1] <- in third second river width is covered by leaves
// 0. |x| | | 1. |x|x| | 2. |x|x| | 3. |x|x|x|
// IN: an integer and an array
// OUT: earliest time, when river can be crossed or -1 if cannot be at all

function frogRiverOne(width, array) {
  const widthArray = new Array(width + 1);
  widthArray.fill(null);
  let score = 0;

  for (let i = 0, len = array.length; i < len; i++) {
    if (widthArray[array[i]] === null) {
      widthArray[array[i]] = array[i];
      score++;
      if (score === width) {
        return i;
      }
    }
  }
  return -1;
}

// const arr1 = [1, 2, 1, 3, 1];
// const arr2 = [1, 2, 1, 3, 1];
//
// console.log(frogRiverOne(3, arr1)); // 3
// console.log(frogRiverOne(4, arr2)); // -1

// 4.3 MaxCounters
// calculate the values of counters
// you are given N counters (X1, X2, X3...N) and non-empty array of operations A[K]
// there are two possible operations:
// if A[K] = X,(1 ≤ X ≤ N) increase value of Xth counter by 1
// if A[K] = N + 1, set all counters to current max value
// For example: N = 4  A = [2, 3, 5, 1, 3]
// Init. (0, 0, 0, 0) 0. (0, 1, 0, 0)
// 1. (0, 1, 1, 0) 2. (1, 1, 1, 1)
// 3. (2, 1, 1, 1) 4. (2, 1, 2, 1)
// IN: a non- empty array A consisting of M integers
// an integer N - number of counters
// N and M are integers within the range [1...100 000]
// each element of array A is an integer within the range [1...N + 1]
// OUT: an array with counters

//TODO: improve performance
function maxCounters(n, array) {
  const counters = new Array(n);
  counters.fill(0);
  let max = counters[0];

  for (let i = 0, len = array.length; i < len; i++) {
    if (array[i] <= n) {
      counters[array[i] - 1]++;
      max = max > counters[array[i] - 1] ? max : counters[array[i] - 1];
    } else {
      counters.fill(max);
    }
  }
  return counters;
}

// console.log(maxCounters(4, [2, 3, 5, 1, 3])); // [2, 1, 2, 1]
// console.log(maxCounters(4, [2])); // [0, 1, 0, 0]
// console.log(maxCounters(4, [5])); // [0, 0, 0, 0]

// 4.4 MissingInteger
// find smallest positive integer missing in array given
// for example [1, 4, 6 ,5] <- 2
// IN: an array of N integers [1...100 000]
// each element of array i within range [-1M...1M]
// OUT: an integer

function missingInteger(array) {
  array.sort((a, b) => a - b);

  const noDuplicates = [...new Set(array)];
  const indexOfOne = noDuplicates.indexOf(1);
  let minInt = 2;

  if (indexOfOne > -1) {
    for (let i = indexOfOne + 1, len = noDuplicates.length; i < len; i++) {
      if (noDuplicates[i] !== minInt) {
        return minInt;
      } else {
        minInt++;
      }
    }
    return minInt;
  } else {
    return 1;
  }
}

// const arr = [1, 3, -6, 4, 1, 2];
// const arr2 = [1, 2, 3];
//
// console.log(missingInteger(arr)); // 5
// console.log(missingInteger(arr2)); // 1
